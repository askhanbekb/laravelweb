<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');
Route::get('/index', 'Controller@index');
Route::post('/send_post', 'AdminController@SendPost')->name('SendPost');
Route::get('/add', 'AdminController@AddPost')->name('AddPost');
//Route::get('/1',[ 
//'as' => '1', 
//'uses' => 'Controller@stocks' 
//]);
Route::get('update/{id}', 'AdminController@update')->name('update');
Route::post('/update_post', 'AdminController@UpdatePost')->name('UpdatePost');
Route::get('delete/{id}', 'AdminController@delete')->name('delete');
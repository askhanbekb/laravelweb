<?php    

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Stock;
use App\Text;

class AdminController extends Controller
{
    
    
     public function SendPost(Request $req){
        $text = new Text();
        $text->name = $req['name'];
        $text->content = $req['text'];
       $text->save();
       return back()->with('succes', 'successfully');
       
    }
    public function update($id){
        $text = Text::find($id);
        return view('update', compact(['text']));
    }
    public function AddPost(){
        return view('add');
    }
    public function UpdatePost(Request $req){
        $text = Text::find($req['id']); 
        $text->name = $req['name'];        
        $text->content = $req['text'];
        $text->save();
        return back()->with('success', 'successfully updated');

    }
    public function delete($id){
        $text = Text::find($id);
        $text ->delete();
        return back()->with('success','text successfully deleted');
    }

}
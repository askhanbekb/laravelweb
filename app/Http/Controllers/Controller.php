<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Request;
use App\User;
use App\Text;
use App\Stock;
use Illuminate\Support\Facades\Input;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function index(){
        $user = User::all();
        $stock=Stock::all();
        $text=Text::all();
        return view('main', compact(['user','stock', 'text']));
    }
    
    
      public function stocks(){
        $user = User::all();
        $stock=Stock::all();
        return view('1', compact(['user','stock']));
    }

}
